package Interface;

public interface Taxable {
	
	public double getTax();
	int compareTo(Object otherObject);

}
