package Interface;
import Model.Company;

public interface Comparator {

	public int compareTo(Object o1, Object o2);
	
}
