package Model;
import Interface.Measurable;
import Interface.Taxable;
public class Person implements Measurable,Taxable,Comparable {
	String name;
	double tall; 
	double income;
	double tax;
	public Person(String name,double tall){
		this.name = name;
		this.tall = tall;
	}
	
	public Person(String name,int income){
		this.name = name;
		this.income = income;
	}
	
	@Override
	public double getMeasurable() {
		return tall;
	}
	
	
	public double getIncome() {
		return income;
	}
	
	public String getName() {
		return name;
	}


	@Override
	public double getTax() {
		if (income <= 300000){
			return (income*5)/100;
		}
		else {
			return 15000+(((income-300000)*10)/100);
		}
	}

	@Override
	public int compareTo(Object otherObject) {
		Person other = (Person) otherObject;
		if (income < other.income){
			return -1;			
		}
		if (income > other.income){
			return 1;			
		}
		return 0;
	}

	public String toString(){
		return this.name;
	}
	
	
}
