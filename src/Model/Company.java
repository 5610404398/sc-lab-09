package Model;
import Interface.Comparator;
import Interface.Taxable;

public class Company implements Taxable,Comparable {
	String name;
	double expense; 
	double income;
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
	}
	@Override
	public double getTax() {
		if (income > expense){
			return ((income-expense)*30)/100;
		}
		else {
			return 0;
		}
		
	}
	
	public double getExpense(){
		return expense;
	}
	

	public double getIncome(){
		return income;
	}
	

	public double getProgfit(){
		return income-expense ;
	}
	
	public String getName(){
	return name;
	}
	
	@Override
	public int compareTo(Object otherObject) {
		Company other = (Company) otherObject;
		if (this.getProgfit() < other.getProgfit()){
			return -1;			
		}
		if (this.getProgfit() > other.getProgfit()){
			return 1;			
		}
		return 0;
	}
	public String toString(){
		return this.name;
	}
}

