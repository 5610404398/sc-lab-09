package Model;

import Interface.Measurable;

public class Country implements Measurable {
	String name;
	double area; 
	public Country(String name,double area){
		this.name = name;
		this.area = area;
	}
	
	@Override
	public double getMeasurable() {
		return area;
	}


}
