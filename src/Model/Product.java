package Model;
import Interface.Taxable;

public class Product  implements Taxable,Comparable{
	String name;
	double price; 

	public Product(String name,double price){
		this.name = name;
		this.price = price;
		
	}
	@Override
	public double getTax() {
		return (price*7)/100;
	}
	
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	
	@Override
	public int compareTo(Object otherObject) {
		Product other = (Product) otherObject;
		if (price < other.price){
			return -1;			
		}
		if (price > other.price){
			return 1;			
		}
		return 0;
	}
	public String toString(){
		return this.name;
	}
}
