package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		double c1 = ((Taxable) o1).getTax();
		double c2 = ((Taxable) o2).getTax();
		if (c1 > c2)
			return 1;
		if (c1 < c2)
			return -1;
		return 0;
	}
}
