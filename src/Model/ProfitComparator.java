package Model;
import java.util.Comparator;
//import Interface.Comparator;

public class ProfitComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		double c1 = ((Company) o1).getProgfit(); 
		double c2 = ((Company) o2).getProgfit(); 
		if (c1 > c2) return 1;
		if (c1 < c2) return -1;
		return 0;
	}
}

	