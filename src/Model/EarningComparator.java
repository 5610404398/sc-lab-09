package Model;

import java.util.Comparator;

public class EarningComparator implements Comparator  {

	@Override
	public int compare(Object o1, Object o2) {
		double c1 = ((Company) o1).getIncome();
		double c2 = ((Company) o2).getIncome(); 
		if (c1 > c2) return 1;
		if (c1 < c2) return -1;
		return 0;
	}

}
