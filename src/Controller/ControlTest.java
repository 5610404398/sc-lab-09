package Controller;
import Interface.Measurable;
import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculater;
import Model.TaxComparator;
import Interface.Taxable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
public class ControlTest {

	public static void main(String[] args) {
		ControlTest test = new ControlTest();
		test.Testcase1();
		test.Testcase2();
		test.Testcase3();
		test.Testcase4();
	}
	
	public void Testcase1(){
		System.out.println("------------------------------");
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Baitoey",7300000)); // --- index 0
		persons.add(new Person("Nich",497840));	// --- index 1
		persons.add(new Person("Pow",1204783)); // --- index 2
		persons.add(new Person("San",12047));  // --- index 3
	
		System.out.println("Compare index0 to index1 : "+persons.get(0).compareTo(persons.get(1)));
		System.out.println("Compare index2 to index1 : "+persons.get(2).compareTo(persons.get(1)));
		System.out.println("Compare index3 to index2 : "+persons.get(3).compareTo(persons.get(2)));
		System.out.println("Compare index1 to index2 : "+persons.get(1).compareTo(persons.get(2)));
		System.out.println("Compare index2 to index3 : "+persons.get(2).compareTo(persons.get(3)));
		System.out.println("Compare index3 to index0 : "+persons.get(3).compareTo(persons.get(0)));
		System.out.println("Compare index3 to index3 : "+persons.get(3).compareTo(persons.get(3)));
		
		Collections.sort(persons);
		System.out.println("\nSort by function");
		for (Person s : persons){
			System.out.print(s.getName()+"--"+s.getIncome()+"  ");
		}
		
	}
	
	public void Testcase2(){
		System.out.println("\n\n------------------------------");
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Fan",2100)); // --- index 0
		products.add(new Product("Champoo",179));	// --- index 1
		products.add(new Product("Food",100)); // --- index 2
		products.add(new Product("Shirt",199));  // --- index 3
	
		System.out.println("Compare index0 to index1 : "+products.get(0).compareTo(products.get(1)));
		System.out.println("Compare index2 to index1 : "+products.get(2).compareTo(products.get(1)));
		System.out.println("Compare index3 to index2 : "+products.get(3).compareTo(products.get(2)));
		System.out.println("Compare index1 to index2 : "+products.get(1).compareTo(products.get(2)));
		System.out.println("Compare index2 to index3 : "+products.get(2).compareTo(products.get(3)));
		System.out.println("Compare index3 to index0 : "+products.get(3).compareTo(products.get(0)));
		System.out.println("Compare index3 to index3 : "+products.get(3).compareTo(products.get(3)));
		
		Collections.sort(products);
		System.out.println("\nSort by function");
		for (Product s : products){
			System.out.print(s.getName()+"--"+s.getPrice()+"  ");
		}
	}
	public void Testcase3(){
		System.out.println("\n\n------------------------------");
			
			ArrayList<Company> companies = new ArrayList<Company>();
			companies.add(new Company("A",2500000,23000)); // --- index 0
			companies.add(new Company("B",100000,50000));	// --- index 1
			companies.add(new Company("C",830000,80000)); // --- index 2
			companies.add(new Company("D",7600000,900000));  // --- index 3
					
			Collections.sort(companies, new EarningComparator());
			System.out.println("\nSort by function income");
			for (Company s : companies){
				System.out.print(s.getName()+"--"+s.getIncome()+"  ");
			}
			
			Collections.sort(companies, new ExpenseComparator());
			System.out.println("\nSort by function expense");
			for (Company s : companies){
				System.out.print(s.getName()+"--"+s.getExpense()+"  ");
			}
			
			Collections.sort(companies, new ProfitComparator());
			System.out.println("\nSort by function profit");
			for (Company s : companies){
				System.out.print(s.getName()+"--"+s.getProgfit()+"  ");
			}
		}

	public void Testcase4(){
		System.out.println("\n\n------------------------------");
			
			ArrayList<Taxable> perprocom = new ArrayList<Taxable>();
			perprocom.add(new Person("Baitoey",2500000)); // --- index 0
			perprocom.add(new Company("VXX",100000,50000));	// --- index 1
			perprocom.add(new Product("Machine",80000)); // --- index 2
			perprocom.add(new Company("Lovelyou",7600000,900000));  // --- index 3
					
			Collections.sort(perprocom, new TaxComparator());
			System.out.println("\nSort by function tax");
			for (Taxable s : perprocom){
				System.out.print(s.toString()+"--"+s.getTax()+" ");
			}
			
			
		}
}


