package Model_Tree;

import java.util.ArrayList;
import java.util.List;

import Interface_Tree.Traversal;

public class PreOrderTraversal implements Traversal {
	ArrayList<String> tree = new ArrayList<String>();
	@Override
	public List<String> traverse(Node root) {
		
		if(root == null) return null;
		  tree.add(root.getNodeValue());
		  traverse(root.leftNode());
		  traverse(root.rightNode()); 
		return tree;
	}
	
	public String toString(){
		return "PreOrderTraversal";
	}

}
