package Model_Tree;

import java.util.ArrayList;
import java.util.List;

import Interface_Tree.Traversal;

public class PostOrderTraversal implements Traversal {
	ArrayList<String> tree = new ArrayList<String>();
	@Override
	public List<String> traverse(Node root) {
		if(root == null) return null;
		
		  traverse( root.leftNode() );
		  traverse( root.rightNode() ); 
		  tree.add(root.getNodeValue());
		
		return tree;
		
	}
	
	public String toString(){
		return "PostOrderTraversal";
	}

}
