package Model_Tree;
public class Node {
	public String str;
	public Node l;
	public Node r;

	public Node(String str, Node l, Node r) {
		this.str = str;
		this.l = l;
		this.r = r;
	}
	
	public Node leftNode() {return l;}
	public Node rightNode() {return r;}
	public String getNodeValue() {return str;}
}
