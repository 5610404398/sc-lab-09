package Model_Tree;

import java.util.ArrayList;
import java.util.List;

import Interface_Tree.Traversal;

public class InOrderTraversal implements Traversal {
	ArrayList<String> tree = new ArrayList<String>();

	public List<String> traverse(Node root) {
		if (root == null) return null;
			traverse(root.leftNode());
			tree.add(root.getNodeValue());
			traverse(root.rightNode());
		return tree;
		}
	
	public String toString(){
		return "InOrderTraversal";
	}
}
