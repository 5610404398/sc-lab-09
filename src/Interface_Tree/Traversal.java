package Interface_Tree;

import java.util.List;

import Model_Tree.Node;

public interface Traversal {
	public List<String> traverse(Node root);
}
