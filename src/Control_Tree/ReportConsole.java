package Control_Tree;

import java.util.ArrayList;

import Interface_Tree.Traversal;
import Model_Tree.Node;

public class ReportConsole {
	ArrayList<String> ans;
	public void display(Node root,Traversal t){
		ans = (ArrayList<String>) t.traverse(root);
		System.out.print(t.toString()+" :: ");
		for (String x : ans){
			System.out.print(x+" ");
		}
	}
}
