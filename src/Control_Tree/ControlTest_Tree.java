package Control_Tree;

import Model_Tree.InOrderTraversal;
import Model_Tree.Node;
import Model_Tree.PostOrderTraversal;
import Model_Tree.PreOrderTraversal;

public class ControlTest_Tree {
	
	public static void main(String[] args) {
		Node a = new Node("A",null,null);
		Node b;
		Node c = new Node("C",null,null);
		Node d;
		Node e = new Node("E",null,null);
		Node f;
		Node g;
		Node h = new Node("H",null,null);
		Node i;
		
		d = new Node("D",c,e);
		i = new Node("I",h,null);
		g = new Node("G",null,i);
		b = new Node("B",a,d);
		f = new Node("F",b,g);
		
		ReportConsole rpc = new ReportConsole();
		rpc.display(f, new InOrderTraversal());
		System.out.println("");
		rpc.display(f, new PreOrderTraversal());
		System.out.println("");
		rpc.display(f, new PostOrderTraversal());
		

	}

}
